package org.thebigmachine.addressdecoder;

/**
 * Created by simon on 4/1/14.
 */
public class Config {

    public static final class location {

        public static final int ACCURACY_DELTA_SIGNIFICANCE_M = 48;
        public static final long BEST_LOCATION_TIME_DELTA_MS = 5 * 60 * 1000;
        public static final int LOCATION_ACCURACY_M = 96;
        public static final int MIN_LOCATION_DISTANCE_M = 500;
        public static final int MIN_LOCATION_TIME_MS = 30000;

    }

}
