package org.thebigmachine.addressdecoder;

import android.location.Location;
import android.util.Log;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by simon on 12/12/14.
 */
public class LatLonParser {

    private static final String TAG = LatLonParser.class.getSimpleName();

    private Pattern mLatLongPattern;

    public String format(Location location) {
        return format(location.getLatitude(), location.getLongitude());
    }

    public String format(double lat, double lng) {
        return String.format("%s, %s", formatLatitude(lat), formatLongitude(lng));
    }

    private String format(double value) {
        double abs_value = Math.abs(value);

        int degrees = (int) abs_value;

        double minsec = (abs_value - degrees) * 60;
        int minutes = (int) minsec;
        double seconds = (minsec - minutes) * 60;

        if (value < 0) {
            degrees = -degrees;
        }

        if (seconds == 0) {
            return String.format("%s°%d’", degrees, minutes);

        } else if (seconds == (int) seconds) {
            return String.format("%d°%d’%d\u201D", degrees, minutes, (int) seconds);

        } else {
            return String.format("%d°%d’%f\u201D", degrees, minutes, seconds);

        }

    }

    public String formatLatitude(double lat) {
        return String.format("%s N", format(lat));
    }

    public String formatLongitude(double lng) {
        return String.format("%s E", format(lng));
    }

    private Pattern initPattern() {
        if (mLatLongPattern == null) {
            // E.g. 38°48′17″N
            // or 77°02′50″W
            // or 15'N
            // or 5″ (N/E is determined by the field)
            // or 17° W
            // or 35°15′
            // etc
            String digits = "\\d+";
            String decimal = "\\d+(?:\\.\\d+)?";
            String degrees = "[º°]";
            String minutes = "['′\u2018\u2019]";
            String seconds = "[\"″\u201C\u201D]";
            String orientation = "[NESW]";

            mLatLongPattern = Pattern.compile(
                    String.format("([+-])?((%s)%s)?((%s)%s)?((%s)%s)?\\s*(%s)?",
                                  digits, degrees,
                                  digits, minutes,
                                  decimal, seconds,
                                  orientation),
                    Pattern.CASE_INSENSITIVE
            );

        }

        return mLatLongPattern;
    }

    public double parse(CharSequence text) throws NumberFormatException {
        if (mLatLongPattern == null) {
            mLatLongPattern = initPattern();
        }

        double value = 0;

        try {
            value = Double.parseDouble((String) text);
            return value;

        } catch (ClassCastException e) {
            return parseViaRegex(text);

        } catch (NumberFormatException e) {
            return parseViaRegex(text);

        }

    }

    private double parseViaRegex(CharSequence text) throws NumberFormatException {
        Matcher matcher = mLatLongPattern.matcher(text);

        Log.d(TAG, String.format("sdd 067; (%s)", text));
        for (int i = 0; i < text.length(); ++i) {
            Log.d(TAG, String.format("sdd 069; (%s) (%d)", text.charAt(i), (int) text.charAt(i)));
        }

        if (!matcher.matches()) {
            throw new NumberFormatException("Invalid input");
        }

        String pos_or_neg = matcher.group(1);
        int sign = 1;

        if ("-".equals(pos_or_neg)) {
            sign = -1;
        }

        double degrees = 0;

        try {
            degrees = Double.parseDouble(matcher.group(3));

        } catch (NullPointerException e) {
            // NOP

        }

        double minutes = 0;

        try {
            minutes = Double.parseDouble(matcher.group(5));

        } catch (NullPointerException e) {
            // NOP

        }

        double seconds = 0;

        try {
            seconds = Double.parseDouble(matcher.group(7));

        } catch (NullPointerException e) {
            // NOP

        }

        String orientation = matcher.group(8);

        double value = sign * (degrees + (minutes / 60) + (seconds / 3600));

        if (orientation != null) {
            // https://portal.fema.gov/FEMAMitigation/fma_help/lat.html
            if ("N".equalsIgnoreCase(orientation)) {
                if (value > 90) {
                    value = 90.0;
                }

            } else if ("S".equalsIgnoreCase(orientation)) {
                if (value > 0) {
                    value = -value;
                }

                if (value < -90) {
                    value = -90.0;
                }

            } else if ("E".equalsIgnoreCase(orientation)) {
                if (value > 180) {
                    value = 180.0;
                }

            } else if ("W".equalsIgnoreCase(orientation)) {
                if (value > 0) {
                    value = -value;
                }

                if (value < -180) {
                    value = -180.0;
                }

            }

        }

        return value;
    }

}
