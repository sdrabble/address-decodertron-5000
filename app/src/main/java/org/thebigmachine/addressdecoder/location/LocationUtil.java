package org.thebigmachine.addressdecoder.location;

import android.location.Location;
import android.util.Log;

import org.thebigmachine.addressdecoder.Config;


public class LocationUtil {

    private static final String TAG = LocationUtil.class.getSimpleName();

    /**
     * @param newLocation
     * @param bestLocationSoFar
     * @return
     */
    public static boolean isBestLocationSoFar(Location newLocation, Location bestLocationSoFar) {
        if (newLocation == null) {
            return false;
        }
        if (bestLocationSoFar == null) {
            return true;
        }

     // Check whether the new location fix is newer or older
        long timeDelta = newLocation.getTime() - bestLocationSoFar.getTime();
        boolean isSignificantlyNewer = timeDelta > Config.location.BEST_LOCATION_TIME_DELTA_MS;
        boolean isSignificantlyOlder = timeDelta < -Config.location.BEST_LOCATION_TIME_DELTA_MS;
        boolean isNewer = timeDelta > 0;

        if (isSignificantlyNewer) {
            Log.d(TAG, String.format("New location is significantly newer than old (%s)", timeDelta));
            return true;

        } else if (isSignificantlyOlder) {
            Log.d(TAG, String.format("New location is significantly older than old (%s)", timeDelta));
            return false;
        }

        int accuracyDelta = (int) (newLocation.getAccuracy() - bestLocationSoFar.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > Config.location.ACCURACY_DELTA_SIGNIFICANCE_M;

        boolean isFromSameProvider = isSameProvider(newLocation.getProvider(),
                                                    bestLocationSoFar.getProvider());

        if (isMoreAccurate) {
            Log.d(TAG, String.format("New location is more accurate than old (%s)", accuracyDelta));
            return true;

        } else if (isNewer && !isLessAccurate) {
            Log.d(TAG, String.format("New location is newer and not less accurate than old (%s)", accuracyDelta));
            return true;

        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            Log.d(TAG, String.format("New location is newer, not significantly less accurate, and from the same provider"));
            return true;

        }
        Log.d(TAG, String.format("New location is worse than old"));
        return false;

    }

    /** Checks whether two providers are the same */
    private static boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
          return provider2 == null;
        }
        return provider1.equals(provider2);
    }

}
