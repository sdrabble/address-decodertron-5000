package org.thebigmachine.addressdecoder.location;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;


public class GeocoderQuery {

    public interface GeocoderListener {
        public void onGeocodeError(String location, Exception exception);
        public void onGeocodeResults(String location, List<Address> results);
    }

    public static final class Point {
        public double mLatitude;
        public double mLongitude;
    }

    private static final String TAG = GeocoderQuery.class.getSimpleName();

    private static final String DOUBLE_RE = "([+-]?\\d+(?:\\.\\d+)?)";
    private static final Pattern LAT_LONG_PATTERN = Pattern.compile(String.format("^%s(?:\\s*([NS]))?[,\\s]\\s*%s(?:\\s*([EW]))?$",
                                                                                  DOUBLE_RE, DOUBLE_RE),
                                                                    Pattern.CASE_INSENSITIVE);

    private Geocoder mGeocoder;
    private Handler mHandler;
    private GeocoderListener mListener;
    private String mLocation;
    private List<Address> mResults;

    public GeocoderQuery(Context context, GeocoderListener listener) {
        mGeocoder = new Geocoder(context);
        mListener = listener;
    }

    public void execute(String locationName) {
        mLocation = locationName;

        mHandler = new Handler();

        new Thread(new Runnable() {

            @Override
            public void run() {
                List<Address> addresses;

                try {
                    Point point = getPointFromLocation(mLocation);

                    if (point != null) {
                        addresses = mGeocoder.getFromLocation(point.mLatitude, point.mLongitude, 5);

                    } else {
                        addresses = mGeocoder.getFromLocationName(mLocation, 5);

                    }

                    if (addresses == null) {
                        addresses = new ArrayList<Address>();
                    }

                    mResults = addresses;

                    onPostExecute(0);

                } catch (IOException e) {
                    mResults = null;
                    e.printStackTrace();
                    onPostExecute(-1);

                }
            }

        }).start();
    }

    public synchronized static final Point getPointFromLocation(String locationName) {
        Matcher matcher = LAT_LONG_PATTERN.matcher(locationName);
        if (matcher.matches()) {
            String ns = matcher.group(2);
            if (TextUtils.isEmpty(ns)) {
                ns = "N";
            }

            String ew = matcher.group(4);
            if (TextUtils.isEmpty(ew)) {
                ew = "E";
            }

            Point point = new Point();
            point.mLatitude = Double.parseDouble(matcher.group(1));
            point.mLongitude = Double.parseDouble(matcher.group(3));

            if (ns.equalsIgnoreCase("S")) {
                point.mLatitude *= -1;
            }

            if (ew.equalsIgnoreCase("W")) {
                point.mLongitude *= -1;
            }

            return point;
        }
        return null;
    }

    protected void onPostExecute(int result) {
        if (mResults != null) {
            final List<Address> addresses = new ArrayList<Address>();
            Log.d(TAG, String.format("sdd 121; (%s)", mResults.size()));
            for (Address address: mResults) {
                addresses.add(address);
            }

            mHandler.post(new Runnable() {

                public void run() {
                    mListener.onGeocodeResults(mLocation, addresses);
                }
            });

        } else {

            mHandler.post(new Runnable() {

                public void run() {
                    mListener.onGeocodeError(mLocation,
                                             new Exception(String.format("Unable to geocode '%s'", mLocation)));
                }
            });

        }
    }

}
