package org.thebigmachine.addressdecoder.location;

import android.location.Location;


public interface Locatable {

    public double getDistance();
    public Location getLocation();
    public void setLocation(Location location);

}
