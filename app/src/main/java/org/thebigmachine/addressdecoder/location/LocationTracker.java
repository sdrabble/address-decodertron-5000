package org.thebigmachine.addressdecoder.location;

import java.util.List;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import org.thebigmachine.addressdecoder.Config;


public class LocationTracker implements LocationListener {

    public interface LocationTrackerListener {
        public void onLocationUpdate(Location location);
    }

    private static final String TAG = LocationTracker.class.getSimpleName();

    private Location mBestLocation;
    private Context mContext;
    private boolean mIsTracking;
    private LocationTrackerListener mListener;
    private LocationManager mLocationManager;

    public LocationTracker(Context context) {
        mContext = context;
        initLocationTracking();
    }

    public synchronized Location getBestLocation() {
        if (!mIsTracking) {
            initLocationTracking();
        }

        return mBestLocation;
    }

    protected void initLocationTracking() {
        mLocationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);

        if (providers != null) {
            for (String provider: providers) {
//                Log.d(TAG, String.format("Requesting location updates from provider %s", provider));
                mLocationManager.requestLocationUpdates(provider,
                                                        Config.location.MIN_LOCATION_TIME_MS,
                                                        Config.location.MIN_LOCATION_DISTANCE_M,
                                                        this);
                Location location = mLocationManager.getLastKnownLocation(provider);
                if (LocationUtil.isBestLocationSoFar(location, mBestLocation)) {
                    mBestLocation = location;
                }
            }

        }
        mIsTracking = true;
    }

    /* (non-Javadoc)
     * @see android.location.LocationListener#onLocationChanged(android.location.Location)
     */
    @Override
    public void onLocationChanged(Location location) {
        if (!LocationUtil.isBestLocationSoFar(location, mBestLocation)) {
            return;
        }
        mBestLocation = location;

        if (mListener != null) {
            mListener.onLocationUpdate(mBestLocation);
        }

        if (location.hasAccuracy() && location.getAccuracy() <= Config.location.LOCATION_ACCURACY_M) {
            Log.d(TAG, String.format("Location is accurate enough; putting the location manager to sleep"));
            mLocationManager.removeUpdates(this);
            mIsTracking = false;
        }
    }

    public void onPause() {
        mLocationManager.removeUpdates(this);
    }

    /* (non-Javadoc)
     * @see android.location.LocationListener#onProviderDisabled(java.lang.String)
     */
    @Override
    public void onProviderDisabled(String provider) {
        // NOP
    }

    /* (non-Javadoc)
     * @see android.location.LocationListener#onProviderEnabled(java.lang.String)
     */
    @Override
    public void onProviderEnabled(String provider) {
        // NOP
    }

    /* (non-Javadoc)
     * @see android.location.LocationListener#onStatusChanged(java.lang.String, int, android.os.Bundle)
     */
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // NOP
    }

    public void onResume() {
        initLocationTracking();
    }

    public void registerForUpdates(LocationTrackerListener listener) {
        mListener = listener;
    }

    public void unregisterForUpdates(LocationTrackerListener listener) {
        mListener = null;
    }

}
