package org.thebigmachine.addressdecoder;

import android.app.Activity;
import android.location.Address;
import android.location.Location;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.thebigmachine.addressdecoder.location.GeocoderQuery;
import org.thebigmachine.addressdecoder.location.LocationTracker;

import java.util.List;


public class MainActivity extends ActionBarActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment
            implements LocationTracker.LocationTrackerListener, GeocoderQuery.GeocoderListener {

        private static final int STYLE_DECIMAL = 0;
        private static final int STYLE_DMS = 1;

        private LocationTracker mLocationTracker;
        private LatLonParser mParser;
        private View mRootView;
        private int mUnitStyle;
        private Location mLocation;

        public PlaceholderFragment() {
            setHasOptionsMenu(true);
            mParser = new LatLonParser();
        }

        private void initLocationTracker() {
            mLocationTracker.registerForUpdates(this);

            mLocation = mLocationTracker.getBestLocation();
            if (mLocation != null) {
                setLocationFields(mLocation);
                performAddressLookup(mLocation);
            }
        }

        private void lookupManualAddress() {
            Toast toast = Toast.makeText(getActivity(), R.string.auto_location_lookup_is_off,
                                         Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 200);
            toast.show();

            mLocationTracker.unregisterForUpdates(this);

            if (mRootView != null) {
                TextView address_field = (TextView) mRootView.findViewById(R.id.address_field);
                String address = address_field.getText().toString();

                performAddressLookup(address);
            }
        }

        private void lookupManualLocation() {
            Toast toast = Toast.makeText(getActivity(), R.string.auto_location_lookup_is_off,
                                         Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 200);
            toast.show();

            mLocationTracker.unregisterForUpdates(this);

            if (mRootView != null) {
                TextView lat_field = (TextView) mRootView.findViewById(R.id.latitude_field);
                TextView lon_field = (TextView) mRootView.findViewById(R.id.longitude_field);
                Location location = parseLocation(lat_field, lon_field);
                updateLocationFields(location);
                performAddressLookup(location);
            }
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);

            mLocationTracker = new LocationTracker(activity);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            mRootView = inflater.inflate(R.layout.fragment_main, container, false);

            initLocationTracker();

            Button button = (Button) mRootView.findViewById(R.id.manual_location_button);
            button.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    lookupManualLocation();
                }
            });

            button = (Button) mRootView.findViewById(R.id.manual_address_button);
            button.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    lookupManualAddress();
                }
            });

            return mRootView;
        }

        @Override
        public void onDetach() {
            mLocationTracker.unregisterForUpdates(this);
            mLocationTracker.onPause();
            mRootView = null;
            super.onDetach();
        }

        @Override
        public void onGeocodeError(String location, Exception exception) {
            exception.printStackTrace();

            if (mRootView != null) {
                TextView address_view = (TextView) mRootView.findViewById(R.id.addresses_field);
                address_view.setText(R.string.error_no_addresses);
            }
        }

        @Override
        public void onGeocodeResults(String location, List<Address> results) {
            if (mRootView != null) {
                StringBuilder data = new StringBuilder();
                for (Address address: results) {
                    data.append(address.toString());
                    data.append("\n-------------------------------------\n");
                }

                TextView address_view = (TextView) mRootView.findViewById(R.id.addresses_field);
                address_view.setText(data.toString());
            }
        }

        @Override
        public void onLocationUpdate(Location location) {
            performAddressLookup(location);
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_restart_gps:
                    Toast toast = Toast.makeText(getActivity(), R.string.auto_location_lookup_is_on,
                                                 Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 200);
                    toast.show();

                    initLocationTracker();
                    return true;

                case R.id.action_toggle_units:
                    toggleUnits();
                    break;

            }

            return false;
        }

        private Location parseLocation(TextView latField, TextView lonField) {
            Location location = new Location("manual");

            String lat = latField.getText().toString();
            String lon = lonField.getText().toString();

            double lat_value = mParser.parse(lat);
            double lon_value = mParser.parse(lon);

            location.setLatitude(lat_value);
            location.setLongitude(lon_value);

            return location;
        }

        private void performAddressLookup(final Location location) {
            String location_name = String.format("%f,%f",
                                                 location.getLatitude(),
                                                 location.getLongitude());
            performAddressLookup(location_name);
        }

        private void performAddressLookup(final String address) {
            GeocoderQuery query = new GeocoderQuery(getActivity(), this);
            query.execute(address);
        }

        private void setLocationFields(Location location) {
            if (mRootView == null) {
                return;
            }

            TextView latitude_view = (TextView) mRootView.findViewById(R.id.latitude_field);
            TextView longitude_view = (TextView) mRootView.findViewById(R.id.longitude_field);

            if (mUnitStyle == STYLE_DECIMAL) {
                latitude_view.setText(String.format("%.6f", location.getLatitude()));
                longitude_view.setText(String.format("%.6f", location.getLongitude()));

            } else {
                latitude_view.setText(mParser.formatLatitude(location.getLatitude()));
                longitude_view.setText(mParser.formatLongitude(location.getLongitude()));

            }
        }

        private void toggleUnits() {
            if (mUnitStyle == STYLE_DECIMAL) {
                mUnitStyle = STYLE_DMS;

            } else {
                mUnitStyle = STYLE_DECIMAL;

            }

            updateLocationFields(mLocation);
        }

        private void updateLocationFields(Location location) {
            setLocationFields(location);
        }
    }
}
