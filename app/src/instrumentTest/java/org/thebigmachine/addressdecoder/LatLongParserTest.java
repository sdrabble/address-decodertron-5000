package org.thebigmachine.addressdecoder;

import android.test.InstrumentationTestCase;


/**
 * Created by simon on 12/12/14.
 */
public class LatLongParserTest extends InstrumentationTestCase {

    private LatLonParser mParser;

    public void setUp() throws Exception {
        super.setUp();

        mParser = new LatLonParser();
    }

    public void testNumericParsing() {
        String value = "3.14159";
        assertEquals(3.14159, mParser.parse(value));
    }

    public void testDegrees() {
        String value = "45°";
        assertEquals(45.0, mParser.parse(value));
    }

    public void testDegreesMinutes() {
        String value = "45°30′";
        assertEquals(45.5, mParser.parse(value));
    }

    public void testDegreesMinutesSeconds() {
        String value = "45°30′15″";
        assertEquals(45.5041666666, mParser.parse(value), 1e-9);
    }

    public void testDegreesMinutesDecimalSeconds() {
        String value = "45°30′15.543″";
        assertEquals(45.5043175, mParser.parse(value), 1e-9);
    }

    public void testDegreesMinutesSecondsAltUnicode() {
        String value = "45°30\u201815\u201D";
        assertEquals(45.5041666666, mParser.parse(value), 1e-9);
    }

    public void testDegreesMinutesSecondsLatitudeNorth() {
        // Valid range for a latitude is -90 to +90
        String value = "105°30′15″ N";
        assertEquals(90.0, mParser.parse(value));
    }

    public void testDegreesMinutesSecondsLongitudeWest() {
        // Valid range for a longitude is -180 to +180
        String value = "195°30′15″ W";
        assertEquals(-180.0, mParser.parse(value));
    }

    public void testDegreesMinutesSecondsLatitudeSouth() {
        // Valid range for a latitude is -90 to +90
        String value = "225°30′15″ S";
        assertEquals(-90.0, mParser.parse(value));
    }

    public void testNegativeDegreesMinutesSeconds() {
        String value = "-105°30′15″ E";
        assertEquals(-105.50416666666666, mParser.parse(value));
    }

}