package org.thebigmachine.addressdecoder;

import android.location.Location;
import android.test.InstrumentationTestCase;


/**
 * Created by simon on 12/12/14.
 */
public class LatLongFormatTest extends InstrumentationTestCase {

    private LatLonParser mParser;

    public void setUp() throws Exception {
        super.setUp();

        mParser = new LatLonParser();
    }

    public void testFormatDegreesMinutesByDoubles() {
        double lat = 35.5;
        double lon = 40.25;
        assertEquals("35°30’ N, 40°15’ E", mParser.format(lat, lon));
    }

    public void testFormatDegreesMinutesSecondsByDoubles() {
        double lat = 35.5625;
        double lon = 40.2542;
        assertEquals("35°33’45” N, 40°15’15.120000” E", mParser.format(lat, lon));
    }

    public void testFormatNegativeDegreesMinutesSecondsByLocation() {
        Location location = new Location("manual");
        location.setLatitude(-60.6060);
        location.setLongitude(-150.4321546457);
        assertEquals("-60°36’21.600000” N, -150°25’55.756725” E", mParser.format(location));
    }

}